package com.myPlants.io.myPlants.Controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.myPlants.io.myPlants.models.Plants;
import com.myPlants.io.myPlants.services.PlantService;

@RestController
public class PlantsController {
	
	@Autowired
	PlantService plantService;

	
	@GetMapping("/AllPlants")
	@Cacheable(value="AllPlants")  
	public List<Plants> AllPlants() {
		
		return plantService.AllPlants();
	}
	
	
	@GetMapping("/Plant")
	public List<Plants> PlantbyState(@RequestParam ("state") String state) {
		
		return plantService.plantbyState(state);
	}
	
	
//	http://localhost:8080/PlantsPageable?page=0&size=3&sort=state
	// can be sorted by state as pageable 
	@GetMapping("/PlantsPageable")
	@Cacheable(value="PlantsPageable")
	public Page<Plants> employeesPageable(Pageable pageable) {
		return plantService.findAll(pageable);

	}
	
	//ability to find plant by id
	@GetMapping("/Plant/{ID}")
	@Cacheable(value="Plant")
	public Optional<Plants> PlantbyID(@PathVariable ("ID") Integer ID) {
		
		return plantService.PlantbyID(ID);
	}
	
	@GetMapping("/topPlants")
	@Cacheable(value="topPlants")
	public List<Plants> topNPlants(@RequestParam ("numberOfPlants") String numberOfPlants,@RequestParam ("mode") String mode ) {
		
		return plantService.topNPlantsbySortOrder(numberOfPlants , mode);
	}
	
	@GetMapping("/bottomPlants")
	@Cacheable(value="bottomPlants")
	public List<Plants> bottomNPlants(@RequestParam ("numberOfPlants") String numberOfPlants,@RequestParam ("mode") String mode ) {
		
		return plantService.bottomNPlantsbySortOrder(numberOfPlants, mode);
	}

}
