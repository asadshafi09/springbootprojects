package com.myPlants.io.myPlants.models;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Plants {

	@Id
	int id;
	String year;
	String state;
	String plantNane;
	String generatorId;
	String generatorStatus;
	String generatorAnualNetGen;

	@Override
	public String toString() {
		return "Plants [id=" + id + ", year=" + year + ", state=" + state + ", plantNane=" + plantNane
				+ ", generatorId=" + generatorId + ", generatorStatus=" + generatorStatus + ", generatorAnualNetGen="
				+ generatorAnualNetGen + "]";
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPlantNane() {
		return plantNane;
	}
	public void setPlantNane(String plantNane) {
		this.plantNane = plantNane;
	}
	public String getGeneratorId() {
		return generatorId;
	}
	public void setGeneratorId(String generatorId) {
		this.generatorId = generatorId;
	}
	public String getGeneratorStatus() {
		return generatorStatus;
	}
	public void setGeneratorStatus(String generatorStatus) {
		this.generatorStatus = generatorStatus;
	}
	public String getGeneratorAnualNetGen() {
		return generatorAnualNetGen;
	}
	public void setGeneratorAnualNetGen(String generatorAnualNetGen) {
		this.generatorAnualNetGen = generatorAnualNetGen;
	}
	
}
