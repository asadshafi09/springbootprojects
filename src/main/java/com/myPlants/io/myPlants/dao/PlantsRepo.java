package com.myPlants.io.myPlants.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.myPlants.io.myPlants.models.Plants;

@Repository
public interface PlantsRepo extends CrudRepository<Plants , Integer>{
	
	public List<Plants> findByState(String state); 
	
	@Query("from Plants Order by ID desc")
	public List<Plants> AllPlantsOrderDesc();
	
	@Query("from Plants Order by ID asc")
	public List<Plants> AllPlantsOrderAsc();
	


}
