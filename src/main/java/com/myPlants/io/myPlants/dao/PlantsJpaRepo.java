package com.myPlants.io.myPlants.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import com.myPlants.io.myPlants.models.Plants;

public interface PlantsJpaRepo extends JpaRepository<Plants, Integer> {
	
	

}
