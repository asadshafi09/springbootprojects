package com.myPlants.io.myPlants.services;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.myPlants.io.myPlants.dao.PlantsJpaRepo;
import com.myPlants.io.myPlants.dao.PlantsRepo;
import com.myPlants.io.myPlants.models.Plants;

@Service
public class PlantService {

	@Autowired
	PlantsRepo plantsrepo;
	
	@Autowired
	PlantsJpaRepo plantsJpaRepo;

	public List<Plants> AllPlants() {

		return (List<Plants>) plantsrepo.findAll();
	}

	public List<Plants> plantbyState(String state) {

		return plantsrepo.findByState(state);
	}
	
	
	public Optional<Plants> PlantbyID(Integer ID) {
		
		return plantsrepo.findById(ID);
	}

	public List<Plants> topNPlantsbySortOrder(String numberOfPlants, String mode) {
		List<Plants> list = Collections.emptyList();
		try {
				list = plantsrepo.AllPlantsOrderAsc();
				list = list.subList(0, Integer.parseInt(numberOfPlants));
				if(mode.equals("DESC")) {
					Collections.reverse(list);
				}
				return list;
		} catch (Exception e) {
			return list;
		}
	}

	public List<Plants> bottomNPlantsbySortOrder(String numberOfPlants, String mode) {
		List<Plants> list = Collections.emptyList();
		try {  
			list = plantsrepo.AllPlantsOrderDesc();			
			list  = list.subList(0, Integer.parseInt(numberOfPlants));
			if(mode.equals("ASC")) {
				Collections.reverse(list);
			}
				return list; 
		} catch (Exception e) {
			return list;
		}
	}

	public Page<Plants> findAll(Pageable pageable) {
		return plantsJpaRepo.findAll(pageable);
	}

}
