package com.myPlants.io.myPlants;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class MyPlantsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyPlantsApplication.class, args);
		
	}

}
