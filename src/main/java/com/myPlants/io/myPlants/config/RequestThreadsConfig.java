package com.myPlants.io.myPlants.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "server", ignoreUnknownFields = true)
public class RequestThreadsConfig {
	public static class Tomcat {
		public static class Threads {
			int max = 1000;
		}
	}
}
