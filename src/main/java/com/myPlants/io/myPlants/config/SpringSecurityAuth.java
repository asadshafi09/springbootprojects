package com.myPlants.io.myPlants.config;

import javax.security.auth.login.LoginException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
 
@Configuration
public class SpringSecurityAuth extends WebSecurityConfigurerAdapter
{
	
	@Value( "${env.mode}" )
	private String envMode;
    @Override
    protected void configure(HttpSecurity http) throws Exception 
    {
    	if(envMode.equals("dev")) {
    	http.authorizeRequests().antMatchers("/").permitAll().antMatchers("/h2-console/**").permitAll();
    	http.csrf().disable();
    	http.headers().frameOptions().disable();
    	}
    	else if(envMode.equals("prod")) {
          http.csrf().disable().authorizeRequests().anyRequest().authenticated().and().httpBasic();
    	}
    }
  
    @Autowired
    public void configure(AuthenticationManagerBuilder auth) throws LoginException 
    {
        try {
			auth.inMemoryAuthentication().withUser("admin").password("{noop}password").roles("admin");
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}