package com.myPlants.io.myPlants.errorController;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class errorController implements ErrorController {
	
	@RequestMapping("/error")
	public String HandleError() { 
		return "error while processing your request ! please try again with valid parameters";
	}

}
